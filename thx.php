<?php
  if ($_POST['password'] != $_POST['passRep']) {
    echo "ERROR: The passwords don't match";
    exit;
  }

  if (!(isset($usrCount))) $usrCount = 0;

  $usrCount++;
  $name1[$usrCount] = $_POST['name1'];
  $name2[$usrCount] = $_POST['name2'];
  $name3[$usrCount] = $_POST['name3'];
  $class[$usrCount] = $_POST['class'];
  $cLetter[$usrCount] = $_POST['cLetter'];
  $login[$usrCount] = $_POST['login'];
  $password[$usrCount] = $_POST['password'];
?>

<html>
  <head>
    <link rel="stylesheet" type="text/css" href="stylez.css">
    <title>THX</title>
  </head>

  <body class="background">
    <table align='center' border=1>
      <tr>
        <td> ID </td>
        <td> First name </td>
        <td> Last name </td>
        <td> Father name </td>
        <td> Class </td>
        <td> Class letter </td>
        <td> Login </td>
      </tr>
      <?php
        for ($tr = 1; $tr <= $usrCount; $tr++){
          echo '<tr>';
              echo '<td>'. $tr .'</td>';
              echo '<td>'. $name1[$tr] .'</td>';
              echo '<td>'. $name2[$tr] .'</td>';
              echo '<td>'. $name3[$tr] .'</td>';
              echo '<td>'. $class[$tr] .'</td>';
              echo '<td>'. $cLetter[$tr] .'</td>';
              echo '<td>'. $login[$tr] .'</td>';
          echo '</tr>';
        }
      ?>

    <button onclick="location.href = 'registration.php';" id=goBack>Back to registration :)</button>
  </body>
